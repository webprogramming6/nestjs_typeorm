import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new user into the database...")
    const productsRepository = AppDataSource.getRepository(Product)
    // const product = new Product()
    // product.name = "Tea"
    // product.price = 60
    // await productsRepository.save(product)
    // console.log("Saved a new product with id: " + product.id)

    // console.log("Loading products from the database...")
    const products = await productsRepository.find()
    console.log("Loaded products: ", products)

    const updateProduct = await productsRepository.findOneBy({id:1})
    console.log(updateProduct)
    updateProduct.price=100
    await productsRepository.save(updateProduct)

}).catch(error => console.log(error))
